import { Head } from "$fresh/runtime.ts";
import IconArrowBack from "icons/tsx/arrow-back.tsx";

export default function NotFound(props: { message: string }) {
  return (
    <>
      <Head>
        <title>Error 404 - Página no encontrada</title>
      </Head>
      <div class="px-4 py-8 mx-auto">
        <div class="max-w-screen-md mx-auto flex flex-col items-center justify-center">
          <h1 class="text-4xl">{props.message}</h1>
          <p class="my-4">
            La página que buscas no existe
          </p>

          <a
            href="/"
            class="px-3 py-2 bg-white rounded border(gray-400 1) hover:bg-gray-200 flex gap-2"
          >
            <IconArrowBack class="w-6 h-6" />Volver
          </a>
        </div>
      </div>
    </>
  );
}
