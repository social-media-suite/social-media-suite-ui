import IconAperture from "icons/tsx/aperture.tsx";

type Props = {
  active: string;
};

export default function Header({ active }: Props) {
  const menus = [
    { name: "Nuevo post", href: "/posts/form" },
  ];

  return (
    <header class="border-t-2 border-gray-200 md:h-16 flex justify-center">
      <div class="bg-white w-full max-w-screen-lg py-4 px-8 flex flex-col md:flex-row gap-4 mx-auto">
        <div class="flex items-center flex-1">
          <IconAperture aria-hidden="true" />
          <div class="text-2xl ml-1 font-bold">
            <a href="/">Social media suite</a>
          </div>
        </div>
        <ul class="flex items-center gap-6">
          {menus.map((menu) => (
            <li>
              <a
                href={menu.href}
                class={"text-gray-500 hover:text-gray-700 py-1 border-gray-500" +
                  (menu.href === active ? " font-bold border-b-2" : "")}
              >
                {menu.name}
              </a>
            </li>
          ))}
        </ul>
      </div>
    </header>
  );
}
