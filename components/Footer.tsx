import { ComponentChildren } from "preact";
import BrandGitlab from "https://deno.land/x/tabler_icons_tsx@0.0.3/tsx/brand-gitlab.tsx";

type Props = {
  children: ComponentChildren;
};

export default function Footer({ children }: Props) {
  const menus = [
    {
      title: "Documentación",
      children: [
        {
          name: "Memoria académica (ES)",
          href:
            "https://social-media-suite-doc-social-media-suite-b753d964522878b91d139.gitlab.io/memoria/",
        },
        {
          name: "Documentación técnica (EN)",
          href:
            "https://social-media-suite-doc-social-media-suite-b753d964522878b91d139.gitlab.io/doc/",
        },
        {
          name: "commits",
          href:
            "https://social-media-suite-doc-social-media-suite-b753d964522878b91d139.gitlab.io/changeset/",
        },
      ],
    },
    {
      title: "API",
      children: [
        {
          name: "Posts API",
          href:
            "https://social-media-suite-api-arturisimo.cloud.okteto.net/posts",
        },
        {
          name: "Open API",
          href:
            "https://social-media-suite-api-arturisimo.cloud.okteto.net/swagger-ui/index.html",
        },
      ],
    },
  ];

  return (
    <footer class="border-t-2 border-gray-200 bg-gray-100 flex justify-center flex-1 mt-auto">
      <div class="bg-gray-100 flex flex-col md:flex-row w-full max-w-screen-lg gap-8 md:gap-16 py-4 px-8 text-xs mx-auto">
        <div class="flex-1">
          <div class="flex items-center gap-1">
            <div class="text-xl">
              Social Media Suite
            </div>
          </div>
        </div>

        {menus.map((item) => (
          <div class="mb-4" key={item.title}>
            <div class="font-bold">{item.title}</div>
            <ul class="mt-2">
              {item.children.map((child) => (
                <li class="mt-2" key={child.name}>
                  <a
                    class="text-gray-500 hover:text-gray-700"
                    href={child.href}
                    target="_blank"
                  >
                    {child.name}
                  </a>
                </li>
              ))}
            </ul>
          </div>
        ))}

        <div class="text-gray-500 space-y-2">
          <a
            href="https://gitlab.com/social-media-suite"
            class="inline-block hover:text-black"
            aria-label="GitHub"
          >
            <BrandGitlab aria-hidden="true" />
          </a>
        </div>
      </div>
    </footer>
  );
}
