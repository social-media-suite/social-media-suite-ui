import type { ServeHandlerInfo } from "$fresh/server.ts";
import { createHandler } from "$fresh/server.ts";
import { assertEquals, assertStringIncludes } from "$std/testing/asserts.ts";
import { load } from "$std/dotenv/mod.ts";
import config from "../fresh.config.ts";
import manifest from "../fresh.gen.ts";
import { DOMParser, Element } from "deno-dom/deno-dom-wasm.ts";

await load({ envPath: ".env.test", export: true });

const url = "http://127.0.0.1:8001/";

const CONN_INFO: ServeHandlerInfo = {
  remoteAddr: { hostname: "127.0.0.1", port: 53496, transport: "tcp" },
};

Deno.test("Home route", async (t) => {
  const handler = await createHandler(manifest, config);

  await t.step("assert returns a 200 status code", async () => {
    const response = await handler(new Request(url), CONN_INFO);
    const { status } = response;
    assertEquals(status, 200);
  });

  await t.step("assert title sets HTML lang attribute", async () => {
    const response = await handler(new Request(url), CONN_INFO);
    const body = await response.text();
    assertStringIncludes(body, `<title>Social Media Suite</title>`);
  });

  await t.step("images have alt attribute", async () => {
    const response = await handler(new Request(url), CONN_INFO);
    const body = await response.text();
    const doc = new DOMParser().parseFromString(body, "text/html")!;
    const images: Element[] = doc.getElementsByTagName(
      "img",
    );

    images.forEach((img) => {
      const altText = img.attributes.getNamedItem("alt")?.value;
      assertEquals(altText && typeof altText, "string");
    });
  });
});
