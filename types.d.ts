interface Post {
  id: string;
  text: string;
  image: string;
  thumb: string;
  added: string;
  updated: string;
}
interface ErrorResponse {
  timestamp: string;
  status: number;
  error: string;
  path: string;
  message: string | undefined;
}
