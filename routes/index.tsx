import { findAllPosts } from "../rest/api-service.ts";
import { Handlers, PageProps } from "$fresh/server.ts";
import type { Post } from "../types.d.ts";
import IconCircleX from "icons/tsx/circle-x.tsx";
import IconCirclePlus from "icons/tsx/circle-plus.tsx";

export const handler: Handlers<Post[]> = {
  async GET(_request, ctx) {
    const posts = await findAllPosts();
    if (posts !== null) {
      return ctx.render(posts);
    } else {
      return ctx.render();
    }
  },
};

export default function Home({ data }: PageProps<Post[] | null>) {
  if (!data) {
    return (
      <h1>
        Empieza por <a href="/form">aquí</a>
      </h1>
    );
  }
  return (
    <>
      <div class="px-4 py-4 mx-auto">
        <div class="max-w-screen-md mx-auto flex flex-col items-center justify-center">
          <section className="columns-7 max-w-7xl mx-auto space-y-4">
            <div class="container mx-auto p-4">
              <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
                {data.map((post: Post) => (
                  <div class="bg-white p-3 rounded-lg shadow-md relative">
                    <a href={`/posts/${post.id}`}>
                      <img
                        src={post.thumb ? post.thumb : post.image}
                        alt={post.text}
                        title={post.text}
                        class="w-full h-auto rounded-sm"
                      />
                    </a>
                    <div class="text-xs">
                      <p class="inline-block align-text-bottom">
                        {`${post.text}`}
                      </p>
                    </div>
                    <div class="p-3 text-xs text-gray-400">
                      <p>
                        <span class="absolute bottom-3 left-2 flex items-center">
                          {Intl.DateTimeFormat("es-ES", {
                            dateStyle: "short",
                            timeStyle: "short",
                          }).format(new Date(post.updated))}
                        </span>
                        <span class="absolute bottom-2 right-2 flex items-center gap-1">
                          <a
                            href={`/posts/${post.id}`}
                            class="hover:bg-gray-200"
                            title="Ampliar"
                          >
                            <IconCirclePlus class="w-5 h-5" />
                          </a>
                          <a
                            href={`/posts/delete?id=${post.id}`}
                            title="Eliminar"
                            class="hover:bg-gray-200"
                          >
                            <IconCircleX class="w-5 h-5" />
                          </a>
                        </span>
                      </p>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </section>
        </div>
      </div>
    </>
  );
}
