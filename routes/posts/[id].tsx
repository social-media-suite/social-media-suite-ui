import { Head } from "$fresh/runtime.ts";
import { deletePostById, findPostById } from "../../rest/api-service.ts";
import { Handlers, PageProps } from "$fresh/server.ts";
import NotFound from "../../components/NotFound.tsx";
import IconCircleX from "icons/tsx/circle-x.tsx";
import IconCircleMinus from "icons/tsx/circle-minus.tsx";

const isObjectEmpty = (objectName: Record<string, string> | Post) => {
  return Object.keys(objectName).length === 0;
};

export const handler: Handlers = {
  async GET(req, ctx) {
    const { id } = ctx.params;
    const post = await findPostById(id);
    return ctx.render(post);
  },
};

export default function form({ data }: PageProps<Post | undefined>) {
  const numberRows = 6;
  if (!data) {
    return (
      <>
        <div class="px-4 py-8 mx-auto">
          <NotFound message="Post no encontrado" />
        </div>
      </>
    );
  }
  const updatedDate = Intl.DateTimeFormat("es-ES", {
    dateStyle: "short",
    timeStyle: "short",
  }).format(new Date(data.updated));
  return (
    <>
      <Head>
        <title>{data.text}</title>
      </Head>
      <div class="px-4 py-8 mx-auto">
        <div class="max-w-screen-md mx-auto flex flex-col items-center justify-center">
          <div class="bg-white p-3 rounded-lg shadow-md relative">
            <img
              src={data.image}
              alt={data.text}
              class="w-full h-auto rounded-sm"
            />
            <div>
              <p class="text-lg">{`${data.text}`}</p>
            </div>
            <div class="p-3 text-xs text-gray-400">
              <p>
                <span class="absolute bottom-3 left-2 flex items-center">
                  {Intl.DateTimeFormat("es-ES", {
                    dateStyle: "short",
                    timeStyle: "short",
                  }).format(new Date(data.updated))}
                </span>
                <span class="absolute bottom-2 right-2 flex items-center gap-2">
                  <a href="/" class="hover:bg-gray-200" title="Ampliar">
                    <IconCircleMinus class="w-5 h-5" />
                  </a>
                  <a
                    href={`/posts/delete?id=${data.id}`}
                    title="Eliminar"
                    class="hover:bg-gray-200"
                  >
                    <IconCircleX class="w-5 h-5" />
                  </a>
                </span>
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
