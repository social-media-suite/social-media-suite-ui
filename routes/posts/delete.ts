import { deletePostById } from "../../rest/api-service.ts";
import { Handlers } from "$fresh/server.ts";

function getUrlParams(url: string): Record<string, string> {
  const params: URLSearchParams = new URL(url).searchParams;
  const po: Record<string, string> = {};
  for (const p of params) {
    po[p[0]] = p[1];
  }
  return po;
}

export const handler: Handlers = {
  async GET(req, _) {
    const params = getUrlParams(req.url);
    const id = params.id;
    await deletePostById(id);
    const headers = new Headers();
    headers.set("location", "/");
    return new Response(null, {
      status: 303,
      headers,
    });
  },
};
