import { Head } from "$fresh/runtime.ts";
import { addPost } from "../../rest/api-service.ts";
import { Handlers, PageProps } from "$fresh/server.ts";
import Error from "../../islands/Error.tsx";
import IconPlus from "icons/tsx/plus.tsx";
import type { ErrorResponse } from "../../types.d.ts";

export const handler: Handlers = {
  GET(_, ctx) {
    return ctx.render();
  },
  async POST(req, ctx) {
    const form = await req.formData();
    const response = await addPost(form);
    if (!response.ok) {
      const error: ErrorResponse = await response.json();
      return ctx.render(error);
    } else {
      const headers = new Headers();
      headers.set("location", "/");
      return new Response(null, {
        status: 303,
        headers,
      });
    }
  },
};

export default function form({ data }: PageProps<ErrorResponse | undefined>) {
  const numberRows = 6;
  if (data) {
    return (
      <>
        <div class="px-4 py-8 mx-auto">
          <div class="max-w-screen-md mx-auto flex flex-col items-center justify-center">
            <Error message={data.message} />
          </div>
        </div>
      </>
    );
  }
  return (
    <>
      <Head>
        <title>Nuevo post Form</title>
      </Head>
      <div class="px-4 py-8 mx-auto">
        <div class="max-w-screen-md mx-auto flex flex-col items-center justify-center">
          <article class="p-4">
            <form
              method="POST"
              class="bg-white p-6 rounded-lg shadow-md"
              enctype="multipart/form-data"
            >
              <div class="mb-4">
                <label for="text" class="block text-gray-700 font-medium">
                  Texto
                </label>
                <textarea
                  name="text"
                  id="text"
                  class="w-full py-2 px-3 border rounded-lg"
                  rows={numberRows}
                >
                </textarea>
              </div>
              <div class="mb-4">
                <label for="image" class="block text-gray-700 font-medium">
                  Imagen
                </label>
                <input
                  class="w-full py-2 px-3 border rounded-lg"
                  type="file"
                  name="image"
                  id="image"
                />
              </div>
              <div class="mb-4 text-center">
                <div class="inline-block rounded relative">
                  <button
                    type="submit"
                    class="px-3 py-2 bg-white rounded border(gray-400 1) hover:bg-gray-200 flex gap-2"
                  >
                    <IconPlus class="w-6 h-6" />Guardar
                  </button>
                </div>
              </div>
            </form>
          </article>
        </div>
      </div>
    </>
  );
}
