import { AppProps } from "$fresh/server.ts";
import Header from "../components/Header.tsx";
import Footer from "../components/Footer.tsx";

export default function App({ Component }: AppProps) {
  return (
    <>
      <html>
        <head>
          <meta charset="utf-8" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0"
          />
          <title>Social Media Suite</title>
        </head>
        <body class="flex flex-col min-h-screen">
          <Header active="true" />
          <Component />
          <Footer children="" />
        </body>
      </html>
    </>
  );
}
