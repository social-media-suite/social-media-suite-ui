import { Head } from "$fresh/runtime.ts";
import NotFound from "../components/NotFound.tsx";

export default function Error404() {
  return (
    <>
      <NotFound message="Página no encontrada" />
    </>
  );
}
