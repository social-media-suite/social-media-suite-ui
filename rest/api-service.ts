import type { Post } from "../types.d.ts";

const apiUrl = Deno.env.get("API_URL") || "http://localhost:8080/posts";

export async function findAllPosts(): Promise<Post[]> {
  try {
    const res = await fetch(apiUrl);
    if (res.status === 404) {
      return [];
    }
    return await res.json();
  } catch (error) {
    console.log(error.message);
    return [];
  }
}

export async function findPostById(id: string): Promise<Post | undefined> {
  try {
    const res = await fetch(apiUrl + "/" + id);
    if (res.status !== 404) {
      return await res.json();
    }
  } catch (error) {
    console.log(error.message);
  }
}

export async function addPost(form: FormData): Promise<Response> {
  return await fetch(apiUrl, {
    method: "POST",
    body: form,
  });
}

export async function deletePostById(id: string) {
  try {
    await fetch(apiUrl + "/" + id, {
      method: "DELETE",
    });
  } catch (error) {
    console.log(error.message);
  }
}
