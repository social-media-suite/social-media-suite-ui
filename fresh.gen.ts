// DO NOT EDIT. This file is generated by Fresh.
// This file SHOULD be checked into source version control.
// This file is automatically updated during development when running `dev.ts`.

import * as $0 from "./routes/_404.tsx";
import * as $1 from "./routes/_app.tsx";
import * as $2 from "./routes/index.tsx";
import * as $3 from "./routes/posts/[id].tsx";
import * as $4 from "./routes/posts/delete.ts";
import * as $5 from "./routes/posts/form.tsx";
import * as $$0 from "./islands/Error.tsx";

const manifest = {
  routes: {
    "./routes/_404.tsx": $0,
    "./routes/_app.tsx": $1,
    "./routes/index.tsx": $2,
    "./routes/posts/[id].tsx": $3,
    "./routes/posts/delete.ts": $4,
    "./routes/posts/form.tsx": $5,
  },
  islands: {
    "./islands/Error.tsx": $$0,
  },
  baseUrl: import.meta.url,
};

export default manifest;
