FROM denoland/deno:alpine
EXPOSE 8000
WORKDIR /app
USER deno
COPY . .
RUN deno cache main.ts
RUN mkdir -p /var/tmp/log
CMD ["run", "--allow-all", "main.ts"]